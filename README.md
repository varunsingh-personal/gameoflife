## Conway's Game of Life

### Demo [here](https://varunsingh-personal.gitlab.io/gameoflife/)

Implements Conway's Game of Life. More about it can be found on [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)


The UI is very simple. There is a Play/Pause control given which pauses/resumes the run. You can mouse-drag on the grid to enter initial state. Clicking on a cell does the same.
