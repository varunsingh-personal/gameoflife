let size = 1000;
let sWidth = size;
let sHeight = size;
let gridLength = 25;
const ALIVE = true;
const DEAD = false;
let renderFrame = 0;

class Cell {
    constructor(x, y) {
        this.currentState = DEAD;
        this.previousState = DEAD;
        this.x = x;
        this.y = y;
    }
}

class GBoard {
    constructor() {
        this.cells = [];
        for (let i = 0; i < gridLength; i++) {
            this.cells[i] = [];
            for (let j = 0; j < gridLength; j++) {
                this.cells[i][j] = new Cell(i, j);
            }
        }
    }

    updateCellState(cell) {

        let x = cell.x;
        let y = cell.y;
        let nCells = [];

        if (x > 0 && y > 0) { nCells.push(this.cells[x - 1][y - 1]); }
        if (x > 0) { nCells.push(this.cells[x - 1][y]); }
        if (x > 0 && y < this.cells[x].length - 1) { nCells.push(this.cells[x - 1][y + 1]); }
        if (y < this.cells[x].length - 1) { nCells.push(this.cells[x][y + 1]); }
        if (x < this.cells.length - 1 && y < this.cells[x].length - 1) { nCells.push(this.cells[x + 1][y + 1]); }
        if (x < this.cells.length - 1) { nCells.push(this.cells[x + 1][y]); }
        if (x < this.cells.length - 1 && y > 0) { nCells.push(this.cells[x + 1][y - 1]); }
        if (y > 0) { nCells.push(this.cells[x][y - 1]); }
        let liveCellsCount = 0;
        nCells.forEach(nCell => {
            if (nCell.previousState === ALIVE) {
                liveCellsCount = liveCellsCount + 1;
            }
        });

        cell.previousState = cell.currentState;
        // console.log("liveCellCount: " + liveCellsCount);
        if (liveCellsCount === 3) {
            cell.currentState = ALIVE;
        } else if (liveCellsCount === 2 || liveCellsCount === 3) {
            cell.currentState = cell.previousState;
        } else {
            cell.currentState = DEAD;
        }
    }

    draw() {
        let cellWidth = size / gridLength;
        let cellHeight = size / gridLength;
        if (play && renderFrame == 0) {
            for (let i = 0; i < gridLength; i++) {
                for (let j = 0; j < gridLength; j++) {
                    this.updateCellState(this.cells[j][i]);
                }
            }
        }
        for (let i = 0; i < gridLength; i++) {
            for (let j = 0; j < gridLength; j++) {
                if (this.cells[j][i].currentState === DEAD) {
                    push();
                    fill(255);
                    stroke(0);
                    rect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
                    pop();
                } else {
                    push();
                    fill(0);
                    stroke(255);
                    rect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
                    pop();
                }
                this.cells[j][i].previousState = this.cells[j][i].currentState;
            }
        }
    }
}

let gBoard = new GBoard();
let play = false;
function setup() {
    sWidth = windowWidth;
    sHeight = windowHeight;
    createCanvas(sWidth, sHeight);
    let playPauseButton = createButton("Play");
    playPauseButton.position(size + (windowWidth - size) / 2 - 250, windowHeight / 2);
    playPauseButton.size(500, 100);
    playPauseButton.mouseClicked(onPlayPauseClicked);
}

function draw() {
    background(220);
    // console.log("called");
    gBoard.draw();
    renderFrame = renderFrame + 1;
    renderFrame = renderFrame % 15;
}

function mouseClicked() {
    if (mouseX < size && mouseY < size) {
        let cellWidth = size / gridLength;
        let cellHeight = size / gridLength;
        rowNum = Math.floor(mouseY / cellWidth);
        colNum = Math.floor(mouseX / cellHeight);
        gBoard.cells[rowNum][colNum].currentState = !gBoard.cells[rowNum][colNum].currentState;
        gBoard.cells[rowNum][colNum].previousState = gBoard.cells[rowNum][colNum].currentState;
        console.log("Cell clicked");
        console.log(gBoard.cells[rowNum][colNum])
    }
}

function onPlayPauseClicked(event) {
    play = !play;
    if (event.srcElement.innerHTML === "Play") {
        console.log(event.srcElement.innerHTML);
        event.srcElement.innerHTML = "Pause";
    } else {
        console.log(event.srcElement.innerHTML);
        event.srcElement.innerHTML = "Play";
    }
}

function mouseDragged() {
    if (mouseX < size && mouseY < size) {
        let cellWidth = size / gridLength;
        let cellHeight = size / gridLength;
        rowNum = Math.floor(mouseY / cellWidth);
        colNum = Math.floor(mouseX / cellHeight);
        if (gBoard.cells[rowNum][colNum].currentState === DEAD) {
            gBoard.cells[rowNum][colNum].currentState = ALIVE;
            gBoard.cells[rowNum][colNum].previousState = ALIVE;
        }
        // gBoard.cells[rowNum][colNum].currentState = !gBoard.cells[rowNum][colNum].currentState;
        // gBoard.cells[rowNum][colNum].previousState = gBoard.cells[rowNum][colNum].currentState;
    }
}